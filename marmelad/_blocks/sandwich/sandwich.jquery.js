var sandwichContent = $('.sandwich__content'),
    downTelegram = $('.app-header__down-telegram'),
    menuTop = $('.menu-top'),
    appHeaderTop = $('.app-header__top'),
    subscribe = $('.app-footer__subscribe-site.hide');


var sandwich = function () {
    $(document).on('click', '.sandwich', function () {
        $(this).parents('.sandwich-wr').toggleClass('sandwich--active');
        $(this).parents('body').toggleClass('overflowBody');
    });
};

sandwich();

function resizeMenu() {
    if ( $(window).width() <= 992 ) {
        downTelegram.appendTo(sandwichContent);
        menuTop.appendTo(sandwichContent);
        subscribe.appendTo(sandwichContent);
    } else {
        downTelegram.appendTo(appHeaderTop).before($('.logo'));
        menuTop.appendTo(appHeaderTop).after($('.app-header__secrets-telegram'));
    }
}
$(window).on('load resize', resizeMenu);

/*sandwichContent.on('click', function (e) {
    if ($(e.target).length) {
        return;
    }
    $(this).toggleClass('opened');
});*/

var tmenu = $('.app-header__top'),
    tmenuOffset = tmenu.offset();
$(window).scroll(function(){
    if (($(window).scrollTop() > tmenuOffset.top)) {
        if (($(window).scrollTop() > tmenuOffset.top)) {
            tmenu.addClass('fixed'); 
        };
    } else {
        tmenu.removeClass('fixed');
    };
}); 
