module.exports = {
  app: {
    lang: 'ru',
    stylus: {
      theme_color: '#3E50B4',
    },
    GA: false, // Google Analytics site's ID
    package: 'ключ перезаписывается значениями из package.json marmelad-сборщика',
    settings: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    storage: 'ключ перезаписывается значениями из файла настроек settings.marmelad.js',
    buildTime: '',
    controls: [
      'default',
      'brand',
      'success',
      'info',
      'warning',
      'danger',
    ],
  },
  pageTitle: 'marmelad',
  menuTop : [
    {
      "title" : "Каналы"
    },
    {
      "title" : "Боты"
    },
    {
      "title" : "Стикеры"
    },
    {
      "title" : "Блог"
    },
    {
      "title" : "Биржа"
    }
  ],
  services : [
    {
      'img' : 'dollar',
      'img_hov': 'dollar',
      'number' : '16700',
      'title' : 'Каналов'
    },
    {
      'img': 'bots',
      'img_hov': 'bots',
      'number' : '1790',
      'title' : 'Ботов'
    },
    {
      'img': 'face',
      'img_hov': 'face',
      'number' : '116726',
      'title' : 'Стикеров'
    },
    {
      'img': 'money',
      'img_hov': 'money',
      'number' : '1546',
      'title' : 'Каналов на бирже'
    }
  ],
  newChannel : [
    {
      'img': 'img/chan1.png',
      'title' : 'Nuances of programming',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций.'
    },
    {
      'img': 'img/chan2.png',
      'title' : 'Автодайджест',
      'text' : 'Автодайжест - это не просто канал. Это выборка клевого автоконтента для вас...'
    },
    {
      'img': 'img/chan3.png',
      'title' : 'Мой фитнес-тренер',
      'text' : 'Уничтожение любых предметов! Мы пускаем вещи под пресс и смотрим, что из этого выйдет!'
    },
    {
      'img': 'img/chan4.png',
      'title' : 'Книги на миллион бизнес',
      'text' : 'Пишу о книгах которые сделали меня миллионером, бери на заметку и у тебя тоже получится.'
    },
    {
      'img': 'img/chan5.png',
      'title' : 'Бизнес Идеи',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций. '
    },
    {
      'img': 'img/chan6.png',
      'title' : 'Nuances of programming',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций.'
    }
  ],
  freshBots : [
    {
      'img': 'img/fresh1.png',
      'title' : 'Твой Teleprofit',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций.'
    },
    {
      'img': 'img/fresh2.png',
      'title' : 'CryptusPrice',
      'text' : 'Автодайжест - это не просто канал. Это выборка клевого автоконтента для вас...'
    },
    {
      'img': 'img/fresh3.png',
      'title' : 'keycobot',
      'text' : 'Уничтожение любых предметов! Мы пускаем вещи под пресс и смотрим, что из этого выйдет!'
    },
    {
      'img': 'img/fresh4.png',
      'title' : 'VtureBot',
      'text' : 'Пишу о книгах которые сделали меня миллионером, бери на заметку и у тебя тоже получится.'
    },
    {
      'img': 'img/fresh5.png',
      'title' : 'Hartve_bot',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций. '
    },
    {
      'img': 'img/fresh6.png',
      'title' : 'BTC CASINO',
      'text' : 'Автомобили без рекламного грима. Самое важное из автомобильных новинок, трендов, тенденций.'
    }
  ],
  newStickers : [
    {
      'img': 'img/stickers-img1.png'
    },
    {
      'img': 'img/stickers-img2.png'
    },
    {
      'img': 'img/stickers-img3.png'
    },
    {
      'img': 'img/stickers-img4.png'
    },
    {
      'img': 'img/stickers-img5.png'
    }
  ],
  blog : [
    {
      'img' : 'img/blog-img1.png',
      'title' : 'Телеграм бот Флибусты — как найти и использовать'
    },
    {
      'img' : 'img/blog-img2.png',
      'title' : 'Telegram для компьютера — скачать бесплатно на русском языке'
    },
    {
      'img' : 'img/blog-img3.png',
      'title' : 'О развитии самобытных Телеграм-сообществ'
    }
  ]
};
